# PycnoCalc #

PycnoCalc is a program used to perform calculations related to Pycnonuclear Reactions.  Pycnonuclear reactions are reactions that take place only in high density Astrophysical environments.  The only places these reactions are thought to take place are in White Dwarfs and Neutron Stars.

### What is this repository for? ###

This repository is used to keep track of issues with PycnoCalc, as well as source code revisions

### Versions ###
* Ver 3.1: Incorporate changes from Barbara Golf and Whitney Ryan (nee Currier) Master Theses

* Ver 3.0: Initial Checkin from Joe Hellmers B.S. Thesis
### For more information Conact: ###
* Joe Hellmers (hellmersjl@icloud.com)